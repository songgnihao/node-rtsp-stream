# node-rtsp-stream rtsp推流服务

#### 介绍
这是一个轻量化的服务，用于推流，如果想到达到自己的请自行修改

#### 软件架构
nodejs 快速搭建的推流服务


#### 安装教程

1.  npm i
2.  下载 FFmpeg 配置环境变量
3.  node serve.js

#### 使用说明

1.  更改你想要获取的rtsp协议摄像头路径，
2.  打开test.html查看摄像
3.  如果想要符合自己要求的，请自行开发，本项目为开源，也可贡献你所更改的代码

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
